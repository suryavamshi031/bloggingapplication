package com.example.blog.payloads;

import lombok.Data;

@Data
public class JwtAutoResponse {

	private String token;
}
